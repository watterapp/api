# WATTER API
Систему API требуется построить на основе REST + JSON. Все запросы типа POST, (даже если по “смыслу” применим GET), данные запросов на сервер будут отправляться POST-параметрами. Основные типы ошибок - стандартные для НТТР-протокола, типа 404, 500 и тп. 

### Вход/Регистрация
* [Проверка номера телефона](/docs/verifyPhone.md)
* [Подтверждение номера телефона](/docs/confirmPhone.md)

### Профиль пользователя
* [Регистрация](/docs/register.md)
* [Загрузка фото](/docs/uploadUserPhoto.md)
* [Получить данные пользователя](/docs/getUserProfile.md)
* [Настройки пользователя](/docs/saveUserSettings.md)
* [Мои фильтры](/docs/getUserFilters.md)
    * [Добавление фильтра](/docs/orderFilter.md)
    * [Запуск/пауза фильтра](/docs/setFilterUsageState.md)
    * [Замена фильтра](/docs/orderFilterChange.md)
* [Мои анализы](/docs/getUserWaterAnalyzes.md)
    * [Заказ анализа воды](/docs/orderWaterAnalisys.md)
* [Пригласить друга](/docs/inviteFriend.md)
* [Cообщения/уведомления](/docs/getUserMessages.md) ``new``

### Каталог фильтров
* [Список фильтров](/docs/getFilters.md) ``new``
* [Фильтр по штрихкоду](/docs/getFilterByBarcode.md)
* [Типы фильтров](/docs/getFilterTypes.md)
* [ТМ фильтров](/docs/getFilterBrands.md) ``new``

### Партнеры
* [Список партнеров](/docs/getPartners.md)

### Города
* [Список городов](/docs/getCities.md)

### Карта воды Украины (начальный экран)
* [Список анализов воды](/docs/getMapWaterAnalyzes.md)
* [Заказ обратного звонка](/docs/orderCallback.md)