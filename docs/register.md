# Регистрация

> Метод подтверждает номера телефона и регистрирует клиента в БД.

Подадрес метода: ``/api/v1/register``

### POST-параметры запроса:

ключ | тип | описание |
------|-|-------------
name | string | ФИО пользователя
birthDate | string | дата рождения пользователя в виде дд.мм.гггг
address | string | адрес проживания пользователя
apiKey | string, requiered | уникальный ключ клиента

### Ответ:
```js
{
    success:1,
    errCode:0,
    errMsg:”text”
}
```

Т.к. данный запрос не подразумевает наличие полезной нагрузки, ключ data не указывается либо под ним возвращается null