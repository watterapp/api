# Список фильтров 

> Метод возвращает список доступных моделей фильтров.

Подадрес метода: ``/api/v1/getFilters``

### POST-параметры запроса:
не обязательно, будут возвращены все модели, по-умолчанию будет отображено 50 моделей, метод принимает:

ключ | тип | описание |
------|-|-------------
page | integer | номер страницы
per_page | integer | кол-во на странице
sortBy | string | сортировка: ``id, name, shows``. id - по-умолчанию
sort | string | порядок сортировки: ``asc, desc``. asc - по-умолчанию
brand | integer | id ТМ, метод [getFilterBrands](/docs/getFilterBrands.md)
type | integer | id типа моделей, метод [getFilterTypes](/docs/getFilterTypes.md)
img_h | integer | высота изображения. по-умолчанию 250px
img_w | integer | ширина изображения. по-умолчанию 250px

### Ответ:
```js
{
    success:1,
    errCode:0,
    errMsg:”text”,
    data: {
          found : 50,
          per_page : 5,
          pages : 10,
          page : 1,
          filters: [{
              id,
              name,
              description,
              imgUrl,
          }]
}
```

Где,

ключ | тип | описание |
------|-|-------------
found | integer | найдено моделей удовлетворяющих выборку
per_page | integer | кол-во на странице
pages | integer | всего страниц
page | integer | текущая страница
filters:id | integer | иникальный идентификатор
filters:name | string | название модели
filters:description | string | краткое описание модели
filters:img | string | изображение модели

