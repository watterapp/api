# Торговые марки (бренды) фильтров

> Метод подтверждает номера телефона и регистрирует клиента в БД.

Подадрес метода: ``/api/v1/getFilterBrands``

### Ответ:
```js
{
    success:1,
    errCode: 0,
    errMsg: ”text”,
    data: {
        brands: {[
           id,
           uid,
           name,
           description,
           icoUrl
        ]}
    }
}
```

Где,

ключ | тип | описание |
------|-|-------------
id | integer | id в БД
uid | string | уникальный идентификатор
name | string | Название бренда
description | string | Описание бренда
img | string | Ссылка на логотип
